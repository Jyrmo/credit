<?php

namespace Jyrmo\Credit\Payout;

use MangoPay\{MangoPayApi, PayOut, Money, PayOutPaymentDetailsBankWire};

class MangoPayPayout implements PayoutInterface {
    /**
     * @var MangoPayApi
     */
    protected $mangoPayApi;

    /**
     * @var string
     */
    protected $authorId;

    /**
     * @var string
     */
    protected $debitedWalletId;

    /**
     * @var string
     */
    protected $currency;

    public function setMangoPayApi(MangoPayApi $mangoPayApi) {
        $this->mangoPayApi = $mangoPayApi;
    }

    public function setAuthorId(string $authorId) {
        $this->authorId = $authorId;
    }

    public function setDebitedWalletId(string $debitedWalletId) {
        $this->debitedWalletId = $debitedWalletId;
    }

    public function setCurrency(string $currency) {
        $this->currency = $currency;
    }

    public function pay(array $params) {
        $payOut = new PayOut();
        $payOut->AuthorId = $this->authorId;
        $payOut->DebitedWalletId = $this->debitedWalletId;
        $funds = new Money();
        $funds->Currency = $this->currency;
        $funds->Amount = $params['amount'];
        $payOut->DebitedFunds = $funds;
        $fees = new Money();
        $fees->Currency = $this->currency;
        $payOut->Fees = $fees;
        $payOut->PaymentType = 'BANK_WIRE';
        $details = new PayOutPaymentDetailsBankWire();
        $details->BankAccountId = $params['bankAccountId'];
        $payOut->MeanOfPaymentDetails = $details;
        $this->mangoPayApi->PayOuts->Create($payOut);
    }
}

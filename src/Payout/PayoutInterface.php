<?php

namespace Jyrmo\Credit\Payout;

interface PayoutInterface
{
    public function pay(array $params);
}

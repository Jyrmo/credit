<?php

namespace Jyrmo\Credit\Charger;

use Stripe\Stripe;
use Stripe\Charge;

class StripeCharger extends AbstractCharger {
    public function setStripeApiKey(string $stripeApiKey) {
        Stripe::setApiKey($stripeApiKey);
    }

    public function __construct(string $stripeApiKey, string $currency) {
        $this->setStripeApiKey($stripeApiKey);
        $this->setCurrency($currency);
    }

    public function charge(array $params) {
        // TODO: exception handling
        // TODO: return/save charge
        // TODO: more params
        $amountCents = 100 * $params['amount'];

        $params = array(
            'amount' => $amountCents,
            'currency' => $this->currency,
            'source' => $params['source'],
            'description' => $params['description'],
        );
        Charge::create($params);
    }
}

<?php

namespace Jyrmo\Credit\Charger;

abstract class AbstractCharger implements ChargerInterface {
    /**
     * @var string
     */
    protected $currency;

    public function setCurrency(string $currency) {
        $this->currency = $currency;
    }
}

<?php

namespace Jyrmo\Credit\Charger;

interface ChargerInterface
{
    public function charge(array $params);
}

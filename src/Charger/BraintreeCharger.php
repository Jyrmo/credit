<?php

namespace Jyrmo\Credit\Charger;

use Braintree\Transaction;

class BraintreeCharger implements ChargerInterface {
    public function charge(array $params) {
        # TODO: read success/failure from result.
        
        Transaction::sale($params);
    }
}
